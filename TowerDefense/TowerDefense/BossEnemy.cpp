#include "BossEnemy.h"

//Added for logging
#include <fstream>
#include "../Logging/Logging/Logging.h"


BossEnemy::BossEnemy()
{
}

BossEnemy::BossEnemy(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed) :
	Enemy(otherHealth, otherDamage, otherSpeed, Boss)
{
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Boss enemy type created! \n", Logger::Level::Info);
	of.close();

	std::cout << "Boss enemy type created! \n";
}

Enemy::SpawnType BossEnemy::getEnemyType() const
{
	return Enemy::Boss;
}

int BossEnemy::getHealthPoints() const
{
	return _healthPoints;
}

uint16_t BossEnemy::getDamage() const
{
	return _damage;
}

uint16_t BossEnemy::getSpeed() const
{
	return _speed;
}


BossEnemy::~BossEnemy()
{
	std::cout << "Boss enemy type destroyed! \n";
}
