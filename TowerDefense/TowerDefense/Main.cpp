#include <SFML/Graphics.hpp>
#include "GameDirector.h"
#include <string>
#include <fstream>

#include "Player.h"

#include "../Logging/Logging/Logging.h"


#define numberOfSquares 45
#define numberOfMovingSquares 19
#define numberOfPlacementSquares 24
#define numberOfStartSquares 1
#define numberOfFinishSquares 1


int main()
{	
	//--------------------------------------------------------------
	//Testing
	/*
	std::string newSomething = "Bei carasu";
	Player thisPlayer(newSomething);

	GameDirector newGameDirector;
	newGameDirector.setPlayer(thisPlayer);
	newGameDirector.getPlayerHealth();
	newGameDirector.autoWaveSpawner();
	newGameDirector.enemyReachPlayer();
	newGameDirector.getPlayerHealth();

	return 0;
	*/
	//--------------------------------------------------------------


	/*std::vector<sf::RectangleShape> vectorOfSquares;
	for(int i = 0; i < numberOfSquares; i++)
	{
		sf::RectangleShape cubix(sf::Vector2f(90.0f, 90.0f));
		cubix.setFillColor(sf::Color::Blue);
		cubix.setOutlineColor(sf::Color::Black);
		cubix.setOutlineThickness(2.0f);
		vectorOfSquares.push_back(cubix);

	}*/

	/*sf::RectangleShape cubix(sf::Vector2f(90.0f, 90.0f));
	cubix.setPosition(50, 50);
	cubix.setFillColor(sf::Color::Yellow);*/



	//------------------------------------------------------------------

	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Started Application...", Logger::Level::Info);


	//started building the map 
	
	GameDirector newGD;
	newGD.menuScreen();

	std::string name = "This";
	Player newPlayer(name);
	//
	//newGD.setMap();
	//newGD.autoWaveSpawner();

	////Testing for logger
	//Enemy* enemyTester = new NormalEnemy(10,10,10);
	//Enemy* enemyTester1 = new StrongEnemy(20,20,20);
	//Enemy* enemyTester2 = new FastEnemy(10,10,10);
	//Enemy* enemyTester3 = new BossEnemy(40,40,40);

	////Testing for turrets
	//Turrets* turretTest = new SmallTurret(10, 10, 10, 10);
	//Turrets* turretTest1 = new MediumTurret(10, 10, 10, 10);
	//Turrets* turretTest2 = new BigTurret(10, 10, 10, 10);
	


	of.close();

	return 0;
}