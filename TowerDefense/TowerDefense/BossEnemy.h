#pragma once
#include "Enemy.h"
class BossEnemy :
	public Enemy
{
public:
	//Special type of enemy, uses superclass builder!
	BossEnemy();
	BossEnemy(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed);

	//Inherited via Enemy
	SpawnType getEnemyType() const override;
	int getHealthPoints() const override;
	uint16_t getDamage() const override;
	uint16_t getSpeed() const override;

	~BossEnemy();
};

