#include "StrongEnemy.h"

//Added for logger
#include <fstream>
#include "../Logging/Logging/Logging.h"


StrongEnemy::StrongEnemy()
{
}

StrongEnemy::StrongEnemy(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed):
	Enemy(otherHealth, otherDamage, otherSpeed, Strong)
{
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Strong enemy type created! \n", Logger::Level::Info);
	of.close();

	std::cout << "Strong enemy type created! \n";
}

Enemy::SpawnType StrongEnemy::getEnemyType() const
{
	return Enemy::Strong;
}

int StrongEnemy::getHealthPoints() const
{
	return _healthPoints;
}

uint16_t StrongEnemy::getDamage() const
{
	return _damage;
}

uint16_t StrongEnemy::getSpeed() const
{
	return _speed;
}


StrongEnemy::~StrongEnemy()
{
	std::cout << "Strong enemy type destroyed! \n";
}
