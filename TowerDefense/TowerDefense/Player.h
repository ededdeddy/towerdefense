#pragma once
#include <string>
#include <cstdint>

class Player
{
private:
	//Entered at the begining of the game.
	std::string _name;

	//Variable money starts at 100 and it will increase when an enemy is killed.(depending on the enemy's type)
	//And it will decrease when a new turret is constructed.(also depending on the turret's type)
	uint16_t _money;

	//Variable health starts at 100 and it will decrease when an enemy hits the base.(depending on the enemy's strenght)
	int _health;

public:
	Player();
	Player(std::string name);
	Player(std::string name, uint16_t money, uint16_t health);
	~Player();

	//Getters
	uint16_t getMoney() const;
	int getHealth() const;
	std::string getName() const;

	//Setters
	void setMoney(uint16_t newMoneyAmount);
	void setHealth(int newHealthPoints);
	void setName(std::string newName);

	void increaseMoney(uint16_t moneyAmount);
	void decreaseMoney(uint16_t moneyAmount);
	void decreaseHealth(int healthPoints);
};