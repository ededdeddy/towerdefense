#include "Enemy.h"
#include<iostream>

const uint16_t kInvalidEnemyVar = NULL;



Enemy::Enemy()
{
	//Default constructor for uninitialized enemy type
	_damage = kInvalidEnemyVar;
	_healthPoints = kInvalidEnemyVar;
	_speed = kInvalidEnemyVar;
	std::cout << "Invalid enemy \n";
}

Enemy::Enemy(int otherHealthPoint, uint16_t otherDamage, uint16_t otherSpeed, SpawnType otherType)
{
	//Constructor for that will be called upon by child classes, based on type of enemy
	this->_healthPoints = otherHealthPoint;
	this->_damage = otherDamage;
	this->_speed = otherSpeed;
	this->_type = otherType;
	std::cout << "Enemy instance created \n";
}

void Enemy::takeDamage(uint16_t ammount)
{
	_healthPoints -= ammount;
}

Enemy::~Enemy()
{
	std::cout << "Enemy instance destroyed! \n";
}