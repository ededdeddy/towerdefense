#include "MediumTurret.h"

#define mediumTurretPrice 100
#define startingAttackSpeed 10
#define startingDamage 3
#define startingRange 10

//Added for logging
#include <fstream>
#include "../Logging/Logging/Logging.h"


MediumTurret::MediumTurret()
{
}

MediumTurret::MediumTurret(uint16_t otherAttackSpeed, uint16_t otherDamage, uint16_t otherRange, uint16_t otherPrice) :
	Turrets(startingAttackSpeed, startingDamage, startingRange, mediumTurretPrice)
{
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Medium turret type created! \n", Logger::Level::Info);
	of.close();

	std::cout << "Spawned medium turret! \n" << std::endl;
}


MediumTurret::~MediumTurret()
{
}

uint16_t MediumTurret::getAttackSpeed() const
{
	return _attackSpeed;
}

uint16_t MediumTurret::getPrice() const
{
	return _price;
}

uint16_t MediumTurret::getDamage() const
{
	return _damage;
}

uint16_t MediumTurret::getRange() const
{
	return _range;
}
