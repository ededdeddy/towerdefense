#pragma once
#include "Enemy.h"
class StrongEnemy :
	public Enemy
{
public:
	StrongEnemy();
	StrongEnemy(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed);

	//Inherited via Enemy
	SpawnType getEnemyType() const override;
	int getHealthPoints() const override;
	uint16_t getDamage() const override;
	uint16_t getSpeed() const override;

	~StrongEnemy();
};

