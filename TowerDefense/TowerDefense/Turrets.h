#pragma once

#include <stdint.h> //for uint16_t

class Turrets
{
protected:

	//Variables for all turrets, ammounts will change based on type for each turret type that will be added later;
	uint16_t _attackSpeed;
	uint16_t _damage;
	uint16_t _range;
	uint16_t _price;


public:
	Turrets();
	Turrets(uint16_t otherAttackSpeed, uint16_t otherDamage, uint16_t otherRange, uint16_t otherPrice);
	 virtual ~Turrets();

	//Getters
	virtual uint16_t getAttackSpeed() const = 0;
	virtual uint16_t getPrice() const = 0;
	virtual uint16_t getDamage() const = 0;
	virtual uint16_t getRange() const = 0;
};
