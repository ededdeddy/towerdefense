#pragma once
#include<string>
#include<iostream>

class Enemy
{
protected:

	//Enemy types, variables will change based on which type is selected
	enum SpawnType {
		Normal,
		Fast,
		Strong,
		Boss
	};

	//Variables for all enemies, ammount will change based on type for each enemy;
	//std::string _name;
	int _healthPoints;
	uint16_t _damage;
	uint16_t _speed;
	SpawnType _type;

public:
	Enemy();
	Enemy(int otherHealthPoint, uint16_t otherDamage, uint16_t otherSpeed, SpawnType otherType);

	//Getters
	virtual SpawnType getEnemyType() const = 0;
	virtual int getHealthPoints() const = 0;
	virtual uint16_t getDamage() const = 0;
	virtual uint16_t getSpeed() const = 0;
	
	void takeDamage(uint16_t ammount);

	virtual ~Enemy();
};
