#include "SmallTurret.h"

#define smallTurretPrice 50
#define startingAttackSpeed 13
#define startingDamage 1
#define startingRange 10

//Added for logging
#include <fstream>
#include "../Logging/Logging/Logging.h"


SmallTurret::SmallTurret()
{
}

SmallTurret::SmallTurret(uint16_t otherAttackSpeed, uint16_t otherDamage, uint16_t otherRange, uint16_t otherPrice):
	Turrets(startingAttackSpeed, startingDamage, startingRange, smallTurretPrice)
{
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Small turret type created! \n", Logger::Level::Info);
	of.close();

	std::cout << "Spawned small turret! \n" << std::endl;
}


SmallTurret::~SmallTurret()
{
}

uint16_t SmallTurret::getAttackSpeed() const
{
	return _attackSpeed;
}

uint16_t SmallTurret::getPrice() const
{
	return _price;
}

uint16_t SmallTurret::getDamage() const
{
	return _damage;
}

uint16_t SmallTurret::getRange() const
{
	return _range;
}
