#include "BigTurret.h"

#define bigTurretPrice 150
#define startingAttackSpeed 10
#define startingDamage 5
#define startingRange 10

//Added for logging
#include <fstream>
#include "../Logging/Logging/Logging.h"

BigTurret::BigTurret()
{
}

BigTurret::BigTurret(uint16_t otherAttackSpeed, uint16_t otherDamage, uint16_t otherRange, uint16_t otherPrice):
	Turrets(startingAttackSpeed, startingDamage, startingRange, bigTurretPrice)
{
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Big turret type created! \n", Logger::Level::Info);
	of.close();

	std::cout << "Spawned big turret! \n" << std::endl;
}


BigTurret::~BigTurret()
{
}

uint16_t BigTurret::getAttackSpeed() const
{
	return _attackSpeed;
}

uint16_t BigTurret::getPrice() const
{
	return _price;
}

uint16_t BigTurret::getDamage() const
{
	return _damage;
}

uint16_t BigTurret::getRange() const
{
	return _range;
}
