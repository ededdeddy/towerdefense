#include "FastEnemy.h"

//Added for logging
#include <fstream>
#include "../Logging/Logging/Logging.h"

FastEnemy::FastEnemy()
{
}

FastEnemy::FastEnemy(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed):
	Enemy(otherHealth, otherDamage, otherDamage, Fast)
{
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Fast enemy type created! \n", Logger::Level::Info);
	of.close();

	std::cout << "Fast enemy type created! \n";
}

Enemy::SpawnType FastEnemy::getEnemyType() const
{
	return Enemy::Fast;
}

int FastEnemy::getHealthPoints() const
{
	return _healthPoints;
}

uint16_t FastEnemy::getDamage() const
{
	return _damage;
}

uint16_t FastEnemy::getSpeed() const
{
	return _speed;
}


FastEnemy::~FastEnemy()
{
	std::cout << "Fast enemy type destroyed! \n";
}
