#pragma once
#include "Enemy.h"


//Enemy subclass for normal type enemy
class NormalEnemy :
	public Enemy
{
public:
	//Same constructors as for superclass
	NormalEnemy();
	NormalEnemy(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed);

	//Inherited via Enemy
	SpawnType getEnemyType() const override;
	int getHealthPoints() const override;
	uint16_t getDamage() const override;
	uint16_t getSpeed() const override;

	~NormalEnemy();
};

