#pragma once
#include "Turrets.h"
#include <iostream>

class BigTurret :
	public Turrets
{
public:
	//same constructor as turrets class
	BigTurret();
	BigTurret(uint16_t otherAttackSpeed, uint16_t otherDamage, uint16_t otherRange, uint16_t otherPrice);
	~BigTurret();

	//inherited via turrets
	uint16_t getAttackSpeed() const override;
	uint16_t getPrice() const override;
	uint16_t getDamage() const override;
	uint16_t getRange() const override;
};

