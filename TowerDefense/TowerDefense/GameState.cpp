#include "GameState.h"

GameState::GameState()
{
}

int GameState::getGameState()
{
	switch (_gameState)
	{
	case Menu:
	{
		return 1;
	}
	case Playing:
	{
		return 2;
	}
	case Finished:
	{
		return 3;
	}
	}
}

void GameState::setGameState(uint16_t otherState)
{
	switch (otherState)
	{
	case 1:
		_gameState = State::Menu;
		break;
	case 2:
		_gameState = State::Playing;
		break;
	case 3:
		_gameState = State::Finished;
		break;
	}
}

GameState::~GameState()
{
}