#include "GameDirector.h"

#include <fstream>
#include <sstream>
#include "../Logging/Logging/Logging.h"

const uint16_t newGame = 1;
const uint16_t newGameStats = 2;
const uint16_t newGameSpawnWave = 5;

#define menu 1
#define playing 2
#define finished 3

//These are defined starting stats/rules for a new game, everything else will be buffed by difficulty
#define normalAndFastSpawnRule 7
#define strongSpawnRule 5
#define bossSpawnRule 1

#define normalAndFastDamage 1
#define strongDamage 2
#define bossDamage 3

#define normalHealthPoints 10
#define fastHealthPoints 5
#define strongHealthPoints 15
#define bossHealthPoints 25

#define normalSpeed 5
#define fastSpeed 7
#define strongSpeed 3
#define bossSpeed 5

#define normalMoneyMultiplier 5
#define fastMoneyMultiplier 5
#define strongMoneyMultiplier 7
#define bossMoneyMultiplier 10


GameDirector::GameDirector()
{
	std::cout << "Game director created! \n";
	this->_wave = newGame;
	this->_difficulty = newGame;
	this->_spawnNumber = newGameSpawnWave;
	this->_enemyHealth = newGameStats;
	this->_enemyDamage = newGameStats;
	this->_enemySpeed = 5;
	this->_enemyBuffer = 0;

	Turrets * invalidTurret = new SmallTurret();
	Turrets * invalidTurret2 = new MediumTurret();
	Turrets * invalidTurret3 = new BigTurret();

	_smallTurretPrice = invalidTurret->getPrice();
	_mediumTurretPrice = invalidTurret2->getPrice();
	_bigTurretPrice = invalidTurret3->getPrice();

	_currentGameState.setGameState(playing);
}



void GameDirector::setMap()
{
	std::ifstream openFile("map.txt");

	sf::Texture tileTexture;
	sf::Sprite tiles;

	sf::Vector2i map[10][10];
	sf::Vector2i loadCounter = sf::Vector2i(0, 0);

	if (openFile.is_open())
	{
		tileTexture.loadFromFile("tiles.png");
		tiles.setTexture(tileTexture);

		while (!openFile.eof())
		{
			std::string str;
			openFile >> str;
			char x = str[0], y = str[2];

			if (!isdigit(x) || !isdigit(y))
				map[loadCounter.x][loadCounter.y] = sf::Vector2i(-1, -1);
			else
				map[loadCounter.x][loadCounter.y] = sf::Vector2i(x - '0', y - '0');

			if (openFile.peek() == '\n')
			{
				loadCounter.x = 0;
				loadCounter.y++;
			}
			else
				loadCounter.x++;
		}
		loadCounter.y++;
	}

	sf::RenderWindow window(sf::VideoMode(1300, 630, 90), "Game on!");

	//-------------------------------------------------------------------------------------------------------
	//-----Score systems -----

	//-----money  system -----


	sf::Font font;
	font.loadFromFile("gameFont.ttf");

	std::ostringstream moneyCounter;
	moneyCounter << "Money: " << _thisPlayer->getMoney();

	sf::Text lblMoney;
	lblMoney.setCharacterSize(27);
	lblMoney.setPosition(965, 25);
	lblMoney.setFillColor(sf::Color::White);
	lblMoney.setFont(font);
	lblMoney.setString(moneyCounter.str());

	//--this is how you call the function
	moneyCounter.str("");
	moneyCounter << "Money: " << _thisPlayer->getMoney();
	lblMoney.setString(moneyCounter.str());

	//-----health system -----

	std::ostringstream healthBar;
	healthBar << "Health: " << _thisPlayer->getHealth();

	sf::Text lblHealth;
	lblHealth.setCharacterSize(27);
	lblHealth.setPosition(965, 55);
	lblHealth.setFillColor(sf::Color::White);
	lblHealth.setFont(font);
	lblHealth.setString(healthBar.str());

	//--this is how you call the function
	healthBar.str("");
	healthBar << "Health: " << _thisPlayer->getHealth();
	lblHealth.setString(healthBar.str());


	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color(18, 18, 18));

		for (int i = 0; i < loadCounter.x; i++)
		{
			for (int j = 0; j < loadCounter.y; j++)
			{
				if (map[i][j].x != -1 && map[i][j].y != -1)
				{
					tiles.setPosition(i * 90, j * 90);
					tiles.setTextureRect(sf::IntRect(map[i][j].x * 90, map[i][j].y * 90, 90, 90));
					window.draw(tiles);
				}
			}
		}

		window.draw(lblMoney);
		window.draw(lblHealth);

		window.display();
	}
}

void GameDirector::menuScreen()
{
	sf::RenderWindow mainMenu(sf::VideoMode(1280, 720), "Welcome to Tower Defense", sf::Style::Close | sf::Style::Titlebar);
	sf::Texture background;
	sf::Sprite backgroundImage;

	//background
	if (!background.loadFromFile("background.jpg"))
		std::cout << "Error could not load background image" << std::endl;

	backgroundImage.setTexture(background);

	//play button
	sf::Texture playButton;
	sf::Sprite playButtonImage;

	playButton.loadFromFile("playbutton.png");
	playButtonImage.setPosition(450, 500);

	playButtonImage.setTexture(playButton);

	//quit button
	sf::Texture quitButton;
	sf::Sprite quitButtonImage;

	quitButton.loadFromFile("quitbutton.png");
	quitButtonImage.setPosition(650, 500);

	quitButtonImage.setTexture(quitButton);

	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Main menu screen opened!", Logger::Level::Info);



	while (mainMenu.isOpen())
	{
		sf::Event evnt;
		while (mainMenu.pollEvent(evnt))
		{
			switch (evnt.type)
			{
			case sf::Event::Closed:
				mainMenu.close();
				break;
			case sf::Event::MouseButtonPressed:

				sf::Vector2i mousePos = sf::Mouse::getPosition(mainMenu);
				sf::Vector2f mousePosF(static_cast<float>(mousePos.x), static_cast<float>(mousePos.y));
				if (playButtonImage.getGlobalBounds().contains(mousePosF));
				{
					std::cout << "Clicked it!" << std::endl;
					if (playButtonImage.getGlobalBounds().contains(mousePosF))
					{
						std::cout << "Game started! \n";
						mainMenu.close();
						gamePlay();
					}
					if (quitButtonImage.getGlobalBounds().contains(mousePosF))
					{
						std::cout << "Game closed! \n";
						mainMenu.close();
					}
				}
				break;
			}
		}
		mainMenu.clear();
		mainMenu.draw(backgroundImage);
		mainMenu.draw(playButtonImage);
		mainMenu.draw(quitButtonImage);
		mainMenu.display();
	}
	logger.log("Main menu screen closed!", Logger::Level::Info);

	of.close();

}

void GameDirector::gamePlay()
{
	std::ifstream openFile("map.txt");

	sf::Texture tileTexture;
	sf::Sprite tiles;

	sf::Vector2i map[10][10];
	sf::Vector2i loadCounter = sf::Vector2i(0, 0);

	//Imported textures for turrets and enemies
	sf::Texture normalEnemyTexture;
	sf::Sprite normalEnemy;
	normalEnemyTexture.loadFromFile("NormalEnemy.png");
	normalEnemy.setTexture(normalEnemyTexture);

	sf::Texture fastEnemyTexture;
	sf::Sprite fastEnemy;
	fastEnemyTexture.loadFromFile("FastEnemy.png");
	fastEnemy.setTexture(fastEnemyTexture);

	sf::Texture strongEnemyTexture;
	sf::Sprite strongEnemy;
	strongEnemyTexture.loadFromFile("StrongEnemy.png");
	strongEnemy.setTexture(strongEnemyTexture);

	sf::Texture bossEnemyTexture;
	sf::Sprite bossEnemy;
	bossEnemyTexture.loadFromFile("BossEnemy.png");
	bossEnemy.setTexture(bossEnemyTexture);

	sf::Texture smallTurretTexture;
	sf::Sprite smallTurret;
	smallTurretTexture.loadFromFile("SmallTurret.png");
	smallTurret.setTexture(smallTurretTexture);

	sf::Texture mediumTurretTexture;
	sf::Sprite mediumTurret;
	mediumTurretTexture.loadFromFile("MediumTurret.png");
	mediumTurret.setTexture(mediumTurretTexture);

	sf::Texture bigTurretTexture;
	sf::Sprite bigTurret;
	bigTurretTexture.loadFromFile("BigTurret.png");
	bigTurret.setTexture(bigTurretTexture);


	if (openFile.is_open())
	{
		tileTexture.loadFromFile("tiles.png");
		tiles.setTexture(tileTexture);

		while (!openFile.eof())
		{
			std::string str;
			openFile >> str;
			char x = str[0], y = str[2];

			if (!isdigit(x) || !isdigit(y))
				map[loadCounter.x][loadCounter.y] = sf::Vector2i(-1, -1);
			else
				map[loadCounter.x][loadCounter.y] = sf::Vector2i(x - '0', y - '0');

			if (openFile.peek() == '\n')
			{
				loadCounter.x = 0;
				loadCounter.y++;
			}
			else
				loadCounter.x++;
		}
		loadCounter.y++;
	}

	sf::RenderWindow window(sf::VideoMode(1300, 630, 90), "Game on!");

	//-------------------------------------------------------------------------------------------------------
	//-----Score systems -----

	//-----money  system -----


	sf::Font font;
	font.loadFromFile("gameFont.ttf");

	std::ostringstream moneyCounter;
	moneyCounter << "Money: " << _thisPlayer->getMoney();

	sf::Text lblMoney;
	lblMoney.setCharacterSize(27);
	lblMoney.setPosition(965, 25);
	lblMoney.setFillColor(sf::Color::White);
	lblMoney.setFont(font);
	lblMoney.setString(moneyCounter.str());

	//--this is how you call the function
	moneyCounter.str("");
	moneyCounter << "Money: " << _thisPlayer->getMoney();
	lblMoney.setString(moneyCounter.str());

	//-----health system -----

	std::ostringstream healthBar;
	healthBar << "Health: " << _thisPlayer->getHealth();

	sf::Text lblHealth;
	lblHealth.setCharacterSize(27);
	lblHealth.setPosition(965, 55);
	lblHealth.setFillColor(sf::Color::White);
	lblHealth.setFont(font);
	lblHealth.setString(healthBar.str());

	//--this is how you call the function
	healthBar.str("");
	healthBar << "Health: " << _thisPlayer->getHealth();
	lblHealth.setString(healthBar.str());


	int count = 1;
	int count1 = 1;


	sf::Clock clock;

	int firstPath = 4;
	int secondPath = 4;
	int thirdPath = 4;
	int fourthPath = 3;
	int fifthPath = 4;
	Enemy * enemy = new BossEnemy(10, 10, 10);
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color(18, 18, 18));

		for (int i = 0; i < loadCounter.x; i++)
		{
			for (int j = 0; j < loadCounter.y; j++)
			{
				if (map[i][j].x != -1 && map[i][j].y != -1)
				{
					tiles.setPosition(i * 90, j * 90);
					tiles.setTextureRect(sf::IntRect(map[i][j].x * 90, map[i][j].y * 90, 90, 90));
					window.draw(tiles);

					if (map[i][j].x == 1 && map[i][j].y == 0 && count1 == 1)
					{
						count1--;
						bigTurret.setPosition(i * 90, j * 90);

					}

					if (map[i][j].x == 0 && map[i][j].y == 0 && count == 1)
					{
						count--;
						bossEnemy.setPosition(i * 90, j * 90);
						window.draw(bossEnemy);
					}

					bool ok = true;
					if (map[i][j].x == 0 && map[i][j].y == 0 && count == 0 && (clock.getElapsedTime().asSeconds() > 0.3))
					{

						//going down
						if (firstPath != 0)
						{
							firstPath--;
							bossEnemy.move(0, 90);
							clock.restart();
						}
						else if (secondPath != 0)
						{
							secondPath--;
							bossEnemy.move(90, 0);
							clock.restart();
						}
						else if (thirdPath != 0)
						{
							thirdPath--;
							bossEnemy.move(0, -90);
							clock.restart();
						}
						else if (fourthPath != 0)
						{
							fourthPath--;
							bossEnemy.move(90, 0);
							clock.restart();
						}
						else if (fifthPath != 0)
						{
							fifthPath--;
							bossEnemy.move(0, 90);
							clock.restart();

						}


						//


					}

					window.draw(bossEnemy);
					window.draw(bigTurret);
				}
			}
		}
		healthBar.str("");
		healthBar << "Health: " << _thisPlayer->getHealth();
		lblHealth.setString(healthBar.str());
		window.draw(lblMoney);
		window.draw(lblHealth);
		window.display();

		if (fifthPath == 0)
		{
			_thisPlayer->decreaseHealth(enemy->getDamage());
			if (_thisPlayer->getHealth() <= 0)
			{
				window.close();
			}
		}

		//if (_thisPlayer->getHealth() > 0)
		//{
		//	//waveSpawner();



		//	moneyCounter.str("");
		//	moneyCounter << "Money: " << _thisPlayer->getMoney();
		//	lblMoney.setString(moneyCounter.str());



		//	window.draw(lblMoney);
		//	window.draw(lblHealth);
		//	window.display();
		//}

	}
}

void GameDirector::waveSpawner()
{
	std::cout << _currentGameState.getGameState();
	//std::cout << playing;
	std::cout << "Player Health: " << _thisPlayer->getHealth() << "\n";
	_enemiesSpawned.clear();


	uint16_t waveCount = _wave % 10;

	if (_enemiesSpawned.empty())
	{
		if (waveCount == 1 || waveCount == 2 || waveCount == 6 || waveCount == 7)
		{
			spawnWave(Normal);
		}
		else if (waveCount == 3 || waveCount == 8)
		{
			spawnWave(Fast);
		}
		else if (waveCount == 5 || waveCount == 9)
		{
			spawnWave(Strong);
		}
		else
		{
			spawnWave(Boss);
		}
		_wave++;

		if (waveCount % 2 == 0)
		{
			increaseDifficulty();
		}

		//system("pause");

	}
}

void GameDirector::spawnWave(SpawnType enemyType)
{
	setNumberOfSpawns(enemyType);
	Enemy * addEntity;

	std::vector<Enemy*> enemiesThisRound;

	if (enemyType == Normal)
	{
		setEnemyHealth(enemyType);
		setEnemyDamage(enemyType);
		setEnemySpeed(enemyType);

		for (uint16_t index = 0; index < _spawnNumber; index++)
		{
			addEntity = spawnNormal(_enemyHealth, _enemyDamage, _enemySpeed);
			enemiesThisRound.push_back(addEntity);
		}
	}

	if (enemyType == Fast)
	{
		setEnemyHealth(enemyType);
		setEnemyDamage(enemyType);
		setEnemySpeed(enemyType);

		for (uint16_t index = 0; index < _spawnNumber; index++)
		{
			addEntity = spawnFast(_enemyHealth, _enemyDamage, _enemySpeed);
			enemiesThisRound.push_back(addEntity);
		}
	}
	if (enemyType == Strong)
	{
		setEnemyHealth(enemyType);
		setEnemyDamage(enemyType);
		setEnemySpeed(enemyType);

		for (uint16_t index = 0; index < _spawnNumber; index++)
		{
			addEntity = spawnStrong(_enemyHealth, _enemyDamage, _enemySpeed);
			enemiesThisRound.push_back(addEntity);
		}
	}

	if (enemyType == Boss)
	{
		setEnemyHealth(enemyType);
		setEnemyDamage(enemyType);
		setEnemySpeed(enemyType);

		for (uint16_t index = 0; index < _spawnNumber; index++)
		{
			addEntity = spawnBoss(_enemyHealth, _enemyDamage, _enemySpeed);
			enemiesThisRound.push_back(addEntity);
		}
	}

	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Wave Spawned!", Logger::Level::Info);

	of.close();

	//In case enemy reaches player
	std::cout << "Wave Spawned \n";
	Enemy* whatToDelete;
	bool enemyInteraction = true;


	//enemyTransition(enemiesThisRound);

	/*switch (enemyInteraction)
	{
	case true:
	{*/
	for (int index = 0; index < _spawnNumber; index++)
	{
		whatToDelete = enemiesThisRound.at(index);
		std::cout << "Enemy damage: " << whatToDelete->getDamage();
		_thisPlayer->decreaseHealth(whatToDelete->getDamage());
		moneyDrop(static_cast<SpawnType>(whatToDelete->getEnemyType()));
		delete whatToDelete;
	}
	/*}
	case false:
	{

	}
	}*/
	enemiesThisRound.clear();

}

void GameDirector::setNumberOfSpawns(SpawnType enemyType)
{
	if (enemyType == Normal)
	{
		_spawnNumber = normalAndFastSpawnRule;
	}
	else if (enemyType == Fast)
	{
		_spawnNumber = normalAndFastSpawnRule;
	}
	else if (enemyType == Strong)
	{
		_spawnNumber = strongSpawnRule;
	}
	else if (enemyType == Boss)
	{
		_spawnNumber = bossSpawnRule;
	}

}


void GameDirector::increaseDifficulty()
{

	if (_wave % 2 == 0)
	{
		_difficulty++;
		_enemyBuffer++;
	}
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Difficulty increased!", Logger::Level::Info);
	of.close();
}

void GameDirector::setEnemyHealth(SpawnType enemyType)
{
	if (enemyType == Normal)
	{
		_enemyHealth = normalHealthPoints + _enemyBuffer;
	}
	else if (enemyType == Fast)
	{
		_enemyHealth = fastHealthPoints + _enemyBuffer;
	}
	else if (enemyType == Strong)
	{
		_enemyHealth = strongHealthPoints + _enemyBuffer;
	}
	else if (enemyType == Boss)
	{
		_enemyHealth = bossHealthPoints + _enemyBuffer;
	}
}

void GameDirector::setEnemyDamage(SpawnType enemyType)
{
	if (enemyType == Normal)
	{
		_enemyDamage = normalAndFastDamage + _enemyBuffer;
	}
	else if (enemyType == Fast)
	{
		_enemyDamage = normalAndFastDamage + _enemyBuffer;
	}
	else if (enemyType == Strong)
	{
		_enemyDamage = strongDamage + _enemyBuffer;
	}
	else if (enemyType == Boss)
	{
		_enemyDamage = bossDamage + _enemyBuffer;
	}
}

void GameDirector::setEnemySpeed(SpawnType enemyType)
{
	if (enemyType == Normal)
	{
		_enemySpeed = normalSpeed;
	}
	else if (enemyType == Fast)
	{
		_enemySpeed = fastSpeed;
	}
	else if (enemyType == Strong)
	{
		_enemySpeed = strongSpeed;
	}
	else if (enemyType == Boss)
	{
		_enemySpeed = bossSpeed;
	}
}

void GameDirector::spawnTurret(TurretType turretType)
{
	if (turretType == Small)
	{
		spawnSmallTurret();
	}

	if (turretType == Medium)
	{
		spawnMediumTurret();
	}

	if (turretType == Big)
	{
		spawnBigTurret();
	}
}

Turrets * GameDirector::spawnSmallTurret()
{
	Turrets* smallTurretSpawn = new SmallTurret();
	return smallTurretSpawn;
}

Turrets * GameDirector::spawnMediumTurret()
{
	Turrets* mediumTurretSpawn = new MediumTurret();
	return mediumTurretSpawn;
}

Turrets * GameDirector::spawnBigTurret()
{
	Turrets* bigTurretSpawn = new BigTurret();
	return bigTurretSpawn;
}

Enemy * GameDirector::spawnStrong(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed)
{
	Enemy * strongSpawn = new StrongEnemy(otherHealth, otherDamage, otherSpeed);
	return strongSpawn;
}

Enemy * GameDirector::spawnNormal(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed)
{
	Enemy * normalSpawn = new NormalEnemy(otherHealth, otherDamage, otherSpeed);
	return normalSpawn;
}

Enemy* GameDirector::spawnFast(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed)
{
	Enemy *fastSpawn = new FastEnemy(otherHealth, otherDamage, otherSpeed);
	return fastSpawn;
}

Enemy* GameDirector::spawnBoss(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed)
{
	Enemy * bossSpawn = new BossEnemy(otherHealth, otherDamage, otherSpeed);
	return bossSpawn;
}

void GameDirector::enemyTakesDamage(uint16_t someAmmount, Enemy * enemy)
{
	enemy->takeDamage(someAmmount);
}

void GameDirector::enemyTransition(std::vector<Enemy*> otherVector)
{
	_enemiesSpawned = otherVector;
}

void GameDirector::moneyDrop(SpawnType enemyType)
{

	if (enemyType == SpawnType::Normal)
		_thisPlayer->increaseMoney(_difficulty * normalMoneyMultiplier);

	if (enemyType == SpawnType::Fast)
		_thisPlayer->increaseMoney(_difficulty * fastMoneyMultiplier);

	if (enemyType == SpawnType::Strong)
		_thisPlayer->increaseMoney(_difficulty * strongMoneyMultiplier);

	if (enemyType == SpawnType::Boss)
		_thisPlayer->increaseMoney(_difficulty * bossMoneyMultiplier);


}

void GameDirector::turretShop(TurretType turretType)
{
	uint16_t playerMoney = _thisPlayer->getMoney();
	uint16_t choice;

	std::cin >> choice;
	switch (choice)
	{

	case 1:
	{
		if (playerMoney < _smallTurretPrice)
		{
			std::cout << "Cant afford! \n";
			break;
		}
		else
		{
			spawnTurret(Small);
			_thisPlayer->decreaseMoney(_smallTurretPrice);
		}
	}
	case 2:
	{
		if (playerMoney < _mediumTurretPrice)
		{
			std::cout << "Cant afford! \n";
			break;
		}
		else
		{
			spawnTurret(Medium);
			_thisPlayer->decreaseMoney(_mediumTurretPrice);
		}
	}
	case 3:
	{
		if (playerMoney < _bigTurretPrice)
		{
			std::cout << "Cant afford! \n";
			break;
		}
		else
		{
			spawnTurret(Big);
			_thisPlayer->decreaseMoney(_bigTurretPrice);
		}
	}
	}
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Bought some turrets!", Logger::Level::Info);

	of.close();
}



GameDirector::~GameDirector()
{
}
