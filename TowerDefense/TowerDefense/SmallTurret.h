#pragma once
#include "Turrets.h"
#include <iostream>

class SmallTurret :
	public Turrets
{
public:
	//same constructors as the turrets class
	SmallTurret();
	SmallTurret(uint16_t otherAttackSpeed, uint16_t otherDamage, uint16_t otherRange, uint16_t otherPrice);
	~SmallTurret();

	//Inherited via Turrets
	uint16_t getAttackSpeed() const override;
	uint16_t getPrice() const override;
	uint16_t getDamage() const override;
	uint16_t getRange() const override;
};

