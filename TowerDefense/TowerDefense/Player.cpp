#include "Player.h"

#include <fstream>
#include "../Logging/Logging/Logging.h"

const std::string kInvalidPlayerName = "Undefined";
const uint16_t kInvalidPlayerMoney = NULL;
const uint16_t kInvalidPlayerHealth = NULL;

const uint16_t kDefaultPlayerMoney = 100;
const uint16_t kDefaultPlayerHealth = 100;

Player::Player()
{
	_name = kInvalidPlayerName;
	_money = kInvalidPlayerMoney;
	_health = kInvalidPlayerHealth;
}

Player::Player(std::string name)
{
	_name = name;
	_money = kDefaultPlayerMoney;
	_health = kDefaultPlayerHealth;

	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Player created!", Logger::Level::Info);
	of.close();
}

Player::Player(std::string name, uint16_t money, uint16_t health)
{
	_name = name;
	_money = money;
	_health = health;
}

Player::~Player()
{
}

uint16_t Player::getMoney() const
{
	return _money;
}

int Player::getHealth() const
{
	return _health;
}

std::string Player::getName() const
{
	return _name;
}

void Player::setMoney(uint16_t newMoneyAmount)
{
	_money = newMoneyAmount;
}

void Player::setHealth(int newHealthPoints)
{
	_health = newHealthPoints;
}


void Player::setName(std::string newName)
{
	_name = newName;
}


void Player::increaseMoney(uint16_t moneyAmount)
{
	_money += moneyAmount;
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Player money increased!", Logger::Level::Info);
	of.close();
}

void Player::decreaseMoney(uint16_t moneyAmount)
{
	_money -= moneyAmount;
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Player money decreased!", Logger::Level::Info);
	of.close();
}



void Player::decreaseHealth(int healthPoints)
{
	_health -= healthPoints;
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Player health decreased!", Logger::Level::Info);
	of.close();
}
