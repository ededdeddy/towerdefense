#pragma once
#include<iostream>
#include<vector>
#include<SFML/Graphics.hpp>
#include<fstream>

#include"Enemy.h"
#include"StrongEnemy.h"
#include"NormalEnemy.h"
#include"FastEnemy.h"
#include"BossEnemy.h"
#include"Player.h"
#include"GameState.h"
#include"Turrets.h"
#include"SmallTurret.h"
#include"MediumTurret.h"
#include"BigTurret.h"


class GameDirector
{
private:

	uint16_t _wave;
	uint16_t _spawnNumber;
	uint16_t _difficulty;

	std::vector<Enemy*> _enemiesSpawned;

	enum SpawnType {
		Normal,
		Fast,
		Strong,
		Boss
	};

	enum TurretType {
		Small,
		Medium,
		Big
	};

	int _enemyHealth;
	uint16_t _enemyDamage;
	uint16_t _enemySpeed;
	uint16_t _enemyBuffer;

	//used for turrets
	uint16_t _turretAttackSpeed;
	uint16_t _turretDamage;
	uint16_t _turretRange;

	Player * _thisPlayer = new Player("name");
	GameState _currentGameState;

	uint16_t _smallTurretPrice;
	uint16_t _mediumTurretPrice;
	uint16_t _bigTurretPrice;

public:

	//This class is used to impose the rules by which the game is played
	//Such as spawning enemies, special events, so on an so forth
	GameDirector();

	uint16_t getDifficulty();

	void gamePlay();
	void waveSpawner();


	void setMap();
	void menuScreen();
	void followPath();

	//These methods are for spawning waves of enemies
	void spawnWave(SpawnType enemyType);
	void setNumberOfSpawns(SpawnType enemyType);

	//methods for giving enemies different stats based on how far the game has progressed
	void increaseDifficulty();

	//Setter for health, damage and speed
	void setEnemyHealth(SpawnType enemyType);
	void setEnemyDamage(SpawnType enemyType);
	void setEnemySpeed(SpawnType enemyType);


	//method to spawn a turret
	void spawnTurret(TurretType turretType);

	//methods to create turrets entities
	Turrets* spawnSmallTurret();
	Turrets* spawnMediumTurret();
	Turrets* spawnBigTurret();

	//methods to create enemy entities
	Enemy* spawnStrong(int otherHealth, uint16_t otherTurretDamage, uint16_t otherSpeed);
	Enemy* spawnNormal(int otherHealth, uint16_t otherTurretDamage, uint16_t otherSpeed);
	Enemy* spawnFast(int otherHealth, uint16_t otherTurretDamage, uint16_t otherSpeed);
	Enemy* spawnBoss(int otherHealth, uint16_t otherTurretDamage, uint16_t otherSpeed);


	//these will be later used in conjunction with the player class
	void enemyTakesDamage(uint16_t someAmmount, Enemy * enemy);

	void enemyTransition(std::vector <Enemy*> otherVector);

	void moneyDrop(SpawnType enemyType);
	void turretShop(TurretType turretType);


	~GameDirector();
};

