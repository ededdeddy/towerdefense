#pragma once
#include "Turrets.h"
#include <iostream>

class MediumTurret :
	public Turrets
{
public:
	//same constructors as turrets class
	MediumTurret();
	MediumTurret(uint16_t otherAttackSpeed, uint16_t otherDamage, uint16_t otherRange, uint16_t otherPrice);
	~MediumTurret();

	//inherited via turrets
	uint16_t getAttackSpeed() const override;
	uint16_t getPrice() const override;
	uint16_t getDamage() const override;
	uint16_t getRange() const override;
};

