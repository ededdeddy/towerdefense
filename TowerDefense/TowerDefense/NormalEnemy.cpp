#include "NormalEnemy.h"

//Added for testing logger
#include <fstream>
#include "../Logging/Logging/Logging.h"


NormalEnemy::NormalEnemy()
{
}

NormalEnemy::NormalEnemy(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed):
	Enemy(otherHealth, otherDamage, otherSpeed, Normal)
{
	std::ofstream of("logger.log", std::ios::app);
	Logger logger(of);

	logger.log("Normal enemy type created! \n", Logger::Level::Info);
	of.close();

	std::cout << "Normal enemy type created! \n";
}

Enemy::SpawnType NormalEnemy::getEnemyType() const
{
	return Enemy::Normal;
}

int NormalEnemy::getHealthPoints() const
{
	return _healthPoints;
}

uint16_t NormalEnemy::getDamage() const
{
	return _damage;
}

uint16_t NormalEnemy::getSpeed() const
{
	return _speed;
}


NormalEnemy::~NormalEnemy()
{
	std::cout << "Normal type enemy destroyed! \n";
}
