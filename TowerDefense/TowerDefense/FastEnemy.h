#pragma once
#include "Enemy.h"
//#include <iostream>


//Enemy subclass for fast type enemy
class FastEnemy :
	public Enemy
{
public:
	//Same type of constructors as those in supperclass
	FastEnemy();
	FastEnemy(int otherHealth, uint16_t otherDamage, uint16_t otherSpeed);

	//Inherited via Enemy
	SpawnType getEnemyType() const override;
	int getHealthPoints() const override;
	uint16_t getDamage() const override;
	uint16_t getSpeed() const override;

	~FastEnemy();
};

