#pragma once
#include<iostream>

class GameState
{
protected:

	//Gamestate for each different screen
	enum State {
		Menu,
		Playing,
		Finished
	};

	State _gameState;

public:
	GameState();

	int getGameState();
	void setGameState(uint16_t otherState);

	~GameState();
};
