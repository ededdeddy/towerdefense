#include "stdafx.h"
#include "CppUnitTest.h"
#include "../MediumTurret.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(MediumTurretTests)
	{
	public:

		TEST_METHOD(TestMethod1)
		{

			//Test for MediumTurret(attackSpeed, damage, range, price) constructor
			uint16_t attackSpeed = 10;
			uint16_t damage = 10;
			uint16_t range = 10;
			uint16_t price = 10;
			MediumTurret mediumTurret(attackSpeed, damage, range, price);

			Assert::IsTrue(attackSpeed == mediumTurret.getAttackSpeed());
			Assert::IsTrue(damage == mediumTurret.getDamage());
			Assert::IsTrue(range == mediumTurret.getRange());
			Assert::IsTrue(price == mediumTurret.getPrice());
		}

	};
}