#include "stdafx.h"
#include "CppUnitTest.h"
#include "../GameDirector.h"
#include "../BigTurret.h"
#include "../MediumTurret.h"
#include "../SmallTurret.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(GameDirectorTests)
	{
	public:

		//TEST_METHOD(setNumberOfSpawns)
		//{
		//	//Failed - not accesible
		//	//Test for setNumberOfSpawns(SpawnType enemyType) setter

		//	int strongSpawnRule = 5;

		//	GameDirector gameDirector;
		//	gameDirector.setNumberOfSpawns(GameDirector::SpawnType::Strong);
		//}

		TEST_METHOD(spawnSmallTurret)
		{

			//Test for spawnSmallTurret() method

			GameDirector gameDirector;
			Turrets* smallTurret = gameDirector.spawnSmallTurret();
			Turrets* anotherSmallTurret = gameDirector.spawnSmallTurret();

			if (!(smallTurret->getDamage() == anotherSmallTurret->getDamage()))
				Assert::Fail();

		}
		TEST_METHOD(spawnMediumTurret)
		{

			//Test for spawnMediumTurret() method

			GameDirector gameDirector;
			Turrets* mediumTurret = gameDirector.spawnMediumTurret();
			Turrets* anotherMediumTurret = gameDirector.spawnMediumTurret();

			if (!(mediumTurret->getDamage() == anotherMediumTurret->getDamage()))
				Assert::Fail();

		}

		TEST_METHOD(spawnBigTurret)
		{

			//Test for spawnBigTurret() method

			GameDirector gameDirector;
			Turrets* bigTurret = gameDirector.spawnBigTurret();
			Turrets* anotherBigTurret = gameDirector.spawnBigTurret();

			if (!(bigTurret->getDamage() == anotherBigTurret->getDamage()))
				Assert::Fail();

		}

		TEST_METHOD(spawnNormal)
		{

			//Test for spawnNormal() method

			int health = 100;
			int damage = 100;
			int speed = 100;

			GameDirector gameDirector;

			Enemy* spawnNormalEnemy = gameDirector.spawnNormal(health, damage, speed);
			Enemy* spawnAnotherNormalEnemy = gameDirector.spawnNormal(health, damage, speed);

			if (!(spawnNormalEnemy->getDamage() == spawnAnotherNormalEnemy->getDamage()))
				Assert::Fail();

		}

		TEST_METHOD(spawnFast)
		{

			//Test for spawnFast() method

			int health = 100;
			int damage = 100;
			int speed = 100;

			GameDirector gameDirector;

			Enemy* spawnFastEnemy = gameDirector.spawnFast(health, damage, speed);
			Enemy* spawnAnotherFastEnemy = gameDirector.spawnFast(health, damage, speed);

			if (!(spawnFastEnemy->getDamage() == spawnAnotherFastEnemy->getDamage()))
				Assert::Fail();

		}

		TEST_METHOD(spawnStrong)
		{

			//Test for spawnStrong() method

			int health = 100;
			int damage = 100;
			int speed = 100;

			GameDirector gameDirector;

			Enemy* spawnStrongEnemy = gameDirector.spawnStrong(health, damage, speed);
			Enemy* spawnAnotherStrongEnemy = gameDirector.spawnStrong(health, damage, speed);

			if (!(spawnStrongEnemy->getDamage() == spawnAnotherStrongEnemy->getDamage()))
				Assert::Fail();

		}

		TEST_METHOD(spawnBoss)
		{

			//Test for spawnBoss() method

			int health = 100;
			int damage = 100;
			int speed = 100;

			GameDirector gameDirector;

			Enemy* spawnBossEnemy = gameDirector.spawnBoss(health, damage, speed);
			Enemy* spawnAnotherBossEnemy = gameDirector.spawnBoss(health, damage, speed);

			if (!(spawnBossEnemy->getDamage() == spawnAnotherBossEnemy->getDamage()))
				Assert::Fail();

		}

		TEST_METHOD(increaseDifficulty)
		{
			GameDirector gameDirector;
			uint16_t firstDifficulty = gameDirector.getDifficulty();
			firstDifficulty++;
			//to increase wave:
			gameDirector.waveSpawner();

			gameDirector.increaseDifficulty();
			Assert::IsTrue(gameDirector.getDifficulty() == firstDifficulty);
		}

	};
}