#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{		
	TEST_CLASS(PlayerTests)
	{
	public:
		
		TEST_METHOD(ConstructorWith3Parameters)
		{

			//Test for Player(name, money, health) constructor
			std::string name = "name";
			uint16_t money = 500;
			uint16_t health = 100;
			Player player(name, money, health);

			Assert::IsTrue(name == player.getName());
			Assert::IsTrue(money == player.getMoney());
			Assert::IsTrue(health == player.getHealth());
		}

		TEST_METHOD(ConstructorWith1Parameter)
		{
			//Test for Player(name) constructor
			std::string name1 = "name";
			Player player(name1);

			Assert::IsTrue(name1 == player.getName());
		}

		TEST_METHOD(IncreaseMoney)
		{

			//Test for increaseMoney() method
			std::string name = "name";
			uint16_t money = 500;
			uint16_t health = 100;
			uint16_t increaseAmountMoney = 30;
			Player player(name, money, health);

			uint16_t increaseValue = money + increaseAmountMoney;
			player.increaseMoney(increaseAmountMoney);
			uint16_t afterIncreasing = player.getMoney();

			Assert::IsTrue(increaseValue == afterIncreasing);
		}

		TEST_METHOD(DecreaseMoney)
		{

			//Test for decreaseMoney() method
			std::string name = "name";
			uint16_t money = 500;
			uint16_t health = 100;
			uint16_t decreaseAmountMoney = 30;
			Player player(name, money, health);

			uint16_t decreaseValue = money - decreaseAmountMoney;
			player.decreaseMoney(decreaseAmountMoney);
			uint16_t afterDecreasing = player.getMoney();

			Assert::IsTrue(decreaseValue == afterDecreasing);
		}

		TEST_METHOD(DecreaseHealth)
		{

			//Test for decreaseHealth() method
			std::string name = "name";
			uint16_t money = 500;
			uint16_t health = 100;
			uint16_t decreaseAmountHealth = 30;
			Player player(name, money, health);

			uint16_t decreaseValue = health - decreaseAmountHealth;
			player.decreaseHealth(decreaseAmountHealth);
			uint16_t afterDecreasing = player.getHealth();

			Assert::IsTrue(decreaseValue == afterDecreasing);
		}
	};
}