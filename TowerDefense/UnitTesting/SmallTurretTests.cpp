#include "stdafx.h"
#include "CppUnitTest.h"
#include "../SmallTurret.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(SmallTurretTests)
	{
	public:

		TEST_METHOD(TestMethod1)
		{

			//Test for SmallTurret(attackSpeed, damage, range, price) constructor
			uint16_t attackSpeed = 10;
			uint16_t damage = 10;
			uint16_t range = 10;
			uint16_t price = 10;
			SmallTurret smallTurret(attackSpeed, damage, range, price);

			Assert::IsTrue(attackSpeed == smallTurret.getAttackSpeed());
			Assert::IsTrue(damage == smallTurret.getDamage());
			Assert::IsTrue(range == smallTurret.getRange());
			Assert::IsTrue(price == smallTurret.getPrice());
		}

	};
}