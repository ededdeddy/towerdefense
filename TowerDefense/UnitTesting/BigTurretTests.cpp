#include "stdafx.h"
#include "CppUnitTest.h"
#include "../BigTurret.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(BigTurretTests)
	{
	public:

		TEST_METHOD(TestMethod1)
		{

			//Test BigTurret(attackSpeed, damage, range, price) constructor
			uint16_t attackSpeed = 10;
			uint16_t damage = 10;
			uint16_t range = 10;
			uint16_t price = 10;
			BigTurret bigTurret(attackSpeed, damage, range, price);

			Assert::IsTrue(attackSpeed == bigTurret.getAttackSpeed());
			Assert::IsTrue(damage == bigTurret.getDamage());
			Assert::IsTrue(range == bigTurret.getRange());
			Assert::IsTrue(price == bigTurret.getPrice());
		}

	};
}