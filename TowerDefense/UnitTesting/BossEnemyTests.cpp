#include "stdafx.h"
#include "CppUnitTest.h"
#include "../BossEnemy.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTesting
{
	TEST_CLASS(BossEnemyTests)
	{
	public:

		TEST_METHOD(ConstructorWith4Params)
		{
			int health = 10;
			uint16_t damage = 10;
			uint16_t speed = 10;
			BossEnemy enemy(health, damage, speed);

			if (enemy.getHealthPoints() == health &&
				enemy.getDamage() == damage &&
				enemy.getSpeed() == speed)
			{
				//....
			}
			else
				Assert::Fail();
		}

	};
}